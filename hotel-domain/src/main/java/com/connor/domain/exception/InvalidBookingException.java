package com.connor.domain.exception;

/**
 * This is the parent class of any invalid booking.  This should be thrown for business logic exceptions only.
 * @author Connors
 *
 */
public class InvalidBookingException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9214259370324879458L;

	/**
	 * Constructor
	 * @param message
	 */
	public InvalidBookingException(String message) {
		super(message);
	}	
}
