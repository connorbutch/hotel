package com.connor.domain.exception;

import java.time.LocalDate;

/**
 * Throw this when a range of dates is invalid.
 * @author Connors
 *
 */
public class InvalidDateRangeException extends InvalidBookingException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7799562100202609132L;
	
	/**
	 * 
	 */
	private final LocalDate startDate;
	
	/**
	 * 
	 */
	private final LocalDate endDate;

	/**
	 * 
	 * @param message
	 * @param startDate
	 * @param endDate
	 */
	public InvalidDateRangeException(String message, LocalDate startDate, LocalDate endDate) {
		super(message);
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}
}