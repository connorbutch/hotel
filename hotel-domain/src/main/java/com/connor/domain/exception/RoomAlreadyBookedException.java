package com.connor.domain.exception;

import java.time.LocalDate;
import java.util.List;

/**
 * Throw this exception if a room is already booked for a time frame.  
 * NOTE: i usually follow the pattern of popular libraries (such as spring jdbc)
 * which declare exceptions in java doc, but make them unchecked to allow dev
 * to handle them where they wish.
 * @author Connors
 *
 */
public class RoomAlreadyBookedException extends InvalidBookingException {

	/**
	 * For serialization.
	 */
	private static final long serialVersionUID = -4258465893474140808L;

	/**
	 * The room id -- not generated until hit db
	 */
	private final int roomId;
	
	/**
	 * The start date of conflict
	 */
	private final LocalDate startOfConflict;
	
	/**
	 * End of conflict
	 */
	private final LocalDate endOfConflict;

	/**
	 * 
	 * @param message
	 * @param roomId
	 * @param startOfConflict
	 * @param endOfConflict
	 */
	public RoomAlreadyBookedException(String message, int roomId, LocalDate startOfConflict, LocalDate endOfConflict) {
		super(message); 
		this.roomId = roomId;
		this.startOfConflict = startOfConflict;
		this.endOfConflict = endOfConflict;
	}

	/**
	 * Getter
	 * @return
	 */
	public int getRoomId() {
		return roomId;
	}

	/**
	 * Getter
	 * @return
	 */
	public LocalDate getStartOfConflict() {
		return startOfConflict;
	}

	/**
	 * Getter
	 * @return
	 */
	public LocalDate getEndOfConflict() {
		return endOfConflict;
	}
}