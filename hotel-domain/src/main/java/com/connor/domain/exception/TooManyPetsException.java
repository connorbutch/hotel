package com.connor.domain.exception;

public class TooManyPetsException extends RuntimeException {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -4325813428625421840L;

	/**
	 * 
	 */
	private final int attemptedNumberOfPets;
	
	/**
	 * Constructor
	 * @param message
	 * @param attemptedNumberOfPets
	 */
	public TooManyPetsException(String message, int attemptedNumberOfPets) {
		super(message);
		this.attemptedNumberOfPets = attemptedNumberOfPets;
	}

	public int getAttemptedNumberOfPets() {
		return attemptedNumberOfPets;
	}	
}