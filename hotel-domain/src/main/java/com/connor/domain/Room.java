package com.connor.domain;

import java.io.Serializable;

/**
 * This class represents a room at the hotel
 * @author Connor Butch
 *
 */
public class Room implements Serializable{

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = -8666704447368748243L;

	/**
	 * Unique id
	 */
	private int id;

	/**
	 * The level (1 or 2 currently, but can be expanded upon)
	 */
	private int roomLevel;

	/**
	 * The number of beds (1,2, or 3 currently, but can be easily adadpted)
	 */
	private int numberOfBeds;

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public int getRoomLevel() {
		return roomLevel;
	}

	public void setRoomLevel(int roomLevel) {
		this.roomLevel = roomLevel;
	}

	public int getNumberOfBeds() {
		return numberOfBeds;
	}

	public void setNumberOfBeds(int numberOfBeds) {
		this.numberOfBeds = numberOfBeds;
	}

	//to simulate a db, we will pretend primary key matching (id) determines equality
	@Override
	public boolean equals(Object other) {
		boolean equals = false;

		if(other != null && other instanceof Room && ((Room) other).getId() == id) {
			equals = true;
		}

		return equals;
	}

	//when override equals, must ovveride hashcode with same set of fields
	@Override
	public int hashCode() {
		return id;
	}
}