package com.connor.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

/**
 * This represents a reservation for a continuous group of nights for at least one room.
 * @author Connors
 *
 */
public class Reservation implements Serializable {
	
	/**
	 * For serialization
	 */
	private static final long serialVersionUID = 3208929492493557084L;

	/**
	 * Unique id
	 */
	private int id;
	
	/**
	 * 
	 */
	private LocalDate startDate;
	
	/**
	 * 
	 */
	private LocalDate endDate;
	
	/**
	 * 
	 */
	private int price;
	
	
	/**
	 * 
	 */
	private List<Room> rooms;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDate getStartDate() {
		return startDate;
	}
	
	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public List<Room> getRooms() {
		return rooms;
	}

	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}	
}