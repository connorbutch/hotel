package com.connor.domain;

import java.time.LocalDate;


/**
 * This interface holds all of the constants needed for the application for a common point of reference
 * @author Connors
 *
 */
public interface IHotelConstants {	
	
	//the query parameters -- defined here so we make sure to use same names in client and server without tying them tightly together
	
	public static final String CHECK_IN_QUERY_PARAM = "checkIn";
	
	public static final String CHECK_OUT_QUERY_PARAM = "checkOut";
	
	public static final String NUMBER_OF_PETS_QUERY_PARAM = "numberOfPets";
	
	public static final String IS_HANDICAPPED_QUERY_PARAM = "isHandicapped";
	
	public static final String NUMBER_OF_BEDS_QUERY_PARAM = "numberOfBeds";

	
}
