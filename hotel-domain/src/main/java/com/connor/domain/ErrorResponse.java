package com.connor.domain;

import java.io.Serializable;

/**
 * This is the body of an error response.
 * @author Connors
 *
 */
public class ErrorResponse implements Serializable {

	private final String message;
	
	public ErrorResponse(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
	
	
}
