package com.connor.domain;

/**
 * Allows us to get price as a function of number of pets or number of beds.  Decouples
 * and keeps functionality out of classes and provides for single point of maintenance.
 * 
 * NOTE: in the future, when we have one api orchestrating, that logic could go there.
 * @author Connors
 *
 */
public class RoomUtil {

	/**
	 * Minimum number of beds
	 */
	private static final int MIN_NUM_BEDS = 1;

	/**
	 * Maximum number of beds
	 */
	private static final int MAX_NUM_BEDS = 3;

	/**
	 * Min # pets
	 */
	private static final int MIN_NUM_PETS = 0;

	/**
	 * Max # pets
	 */
	private static final int MAX_NUM_PETS = 2;

	/**
	 * Return the price for a certain number of beds.
	 * @param numberOfBeds
	 * @return
	 */
	public int getPriceForBeds(int numberOfBeds) {
		//throw exception if not in allowed range
		if(numberOfBeds < MIN_NUM_BEDS || numberOfBeds > MAX_NUM_BEDS) {
			throw new IllegalArgumentException(String.format("Num beds must be between %d and %d", MIN_NUM_BEDS, MAX_NUM_BEDS));
		}

		//if in allowed range, compute and return
		return 25 + 25 * numberOfBeds;
	}

	/**
	 * Get price for bringing in pets
	 * @param numberOfPets
	 * @return
	 */
	public int getPriceAddOnForPets(int numberOfPets) {
		//throw exception if not in allowed range
		if(numberOfPets < MIN_NUM_PETS || numberOfPets > MAX_NUM_PETS) {
			throw new IllegalArgumentException(String.format("Number of pets must be between %d and %d", MIN_NUM_PETS, MAX_NUM_PETS));
		}

		//if in allowed range, compute and return
		return 20 * numberOfPets;
	}

	/**
	 * Since the number of pets allowed could change in the future, leave here to make it easy to change:)
	 * @param room
	 * @return
	 */
	public int getNumberOfAllowedPets(Room room) {
		//null check
		if(room == null) {
			throw new IllegalArgumentException("Room can't be null here");
		}

		//Min for first floor, max for second (as it currently stands)
		return (room.getRoomLevel() == 1)? MIN_NUM_PETS: MAX_NUM_PETS;		
	}	

	/**
	 * By extracting this, can easily change in future
	 * @param room
	 * @return
	 */
	public boolean isHandicapAccessible(Room room) {
		//quick check
		if(room == null) {
			throw new IllegalArgumentException("Can't be null");
		}

		return room.getRoomLevel() == 1;
	}
}