package com.connor.hotel.rest;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.inject.*;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.connor.domain.ErrorResponse;
import com.connor.domain.IHotelConstants;
import com.connor.domain.Reservation;
import com.connor.domain.exception.InvalidDateRangeException;
import com.connor.domain.exception.TooManyPetsException;
import com.connor.service.OriginalService;


/**
 * This class contains the entry point for the RESTful (jaxrs) webservice exposed for booking rooms.
 * @author Connors
 *
 */
@ApplicationPath("/") //use the blank context root
public class RoomAndReservationApi extends Application {

	/**
	 * Prefer constructor injection, but resteasy (implementation of jaxrs) doesn't allow 
	 */
	@Inject
	private OriginalService service;

	//override resteasy providers and singletons to register this class

	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> classes = new HashSet<>();
		classes.add(this.getClass());
		return Collections.unmodifiableSet(classes);
	}

	@Override
	public Set<Object> getSingletons() {
		Set<Object> singletons = new HashSet<>();
		singletons.add(this);
		return Collections.unmodifiableSet(singletons);
	}

	/**
	 * Rest method that makes a reservation and returns it if possible.  Otherwise, will return null entity.
	 * @param checkIn
	 * @param checkout
	 * @param numberOfPets
	 * @param isHandicapped
	 * @param numberOfBeds
	 * @return
	 */
	@GET //get request
	@Produces(MediaType.APPLICATION_JSON) //returns object in json format
	public Response makeReservation(@QueryParam(IHotelConstants.CHECK_IN_QUERY_PARAM) LocalDate checkIn, @QueryParam(IHotelConstants.CHECK_OUT_QUERY_PARAM) LocalDate checkout, @QueryParam(IHotelConstants.NUMBER_OF_PETS_QUERY_PARAM) int numberOfPets, @QueryParam(IHotelConstants.IS_HANDICAPPED_QUERY_PARAM) boolean isHandicapped, @QueryParam(IHotelConstants.NUMBER_OF_BEDS_QUERY_PARAM) int numberOfBeds) {
		//what we return -- body is contained in entity and http status code in status
		Response response = null;
		try {
			//make the reservation, and if successful (even if we couldn't book a room), return it in the response body
			Reservation reservation = service.placeReservation(checkIn, checkout, numberOfPets, isHandicapped, numberOfBeds);
			response = Response.status(Status.OK).entity(reservation).build();
		}
		//this means they entered an invalid date range
		catch(InvalidDateRangeException e) {
			ErrorResponse errorResponse = new ErrorResponse("Please enter valid date range");
			response = Response.status(Status.BAD_REQUEST).entity(errorResponse).build();
		}
		//this means they tried to bring too many pets
		catch(TooManyPetsException e) {
			ErrorResponse errorResponse = new ErrorResponse("Please reduce your number of pets."); //better error message in real life, as this is customer facing.
			response = Response.status(Status.BAD_REQUEST).entity(errorResponse).build();
		}
		//NOTE: didn't want to validate number of beds in case that changed . . . could do here after business input

		//return the created response, regardless or not if it had valid data or not
		return response;
	}	
}