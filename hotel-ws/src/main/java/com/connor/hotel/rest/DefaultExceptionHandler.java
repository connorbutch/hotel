package com.connor.hotel.rest;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.connor.domain.ErrorResponse;

/**
 * This class handles any uncaught exceptions.  Please always try to catch exceptions and not rely on this.
 * @author Connors
 *
 */
//@Provider
public class DefaultExceptionHandler implements ExceptionMapper<Throwable> {

	public Response toResponse(Throwable exception) {
		ErrorResponse errorResponse = new ErrorResponse("An unknown error occurred");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(errorResponse).build();
	}
}