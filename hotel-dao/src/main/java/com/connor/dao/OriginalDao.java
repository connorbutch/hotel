package com.connor.dao;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import com.connor.domain.Reservation;
import com.connor.domain.Room;

/**
 * This contains the contract for all currently implemented functionality.  In the future, move away from this
 * towards more specific DAO
 * @author Connors
 *
 */
public interface OriginalDao extends Serializable {

	/**
	 * Place a reservation for rooms for a given number of nights
	 * @param rooms
	 * @param checkIn
	 * @param checkout
	 * @throws RoomAlreadyBookedException
	 * @return
	 */
	public Reservation placeReservation(List<Room> rooms, LocalDate checkIn, LocalDate checkout, int numberOfPets);
	
	
	/**
	 * 
	 * @param checkIn
	 * @param checkout
	 * @return
	 */
	public List<Room> getAvailableRooms(LocalDate checkIn, LocalDate checkout);
	
	/**
	 * 
	 * @param checkIn
	 * @param checkout
	 * @param isHandicapped
	 * @return
	 */
	public List<Room> getAvailableRooms(LocalDate checkIn, LocalDate checkout, boolean isHandicapped);
	
	/**
	 * 
	 * @param checkIn
	 * @param checkout
	 * @param isHandicapped
	 * @param numberOfPets
	 * @return
	 */
	public List<Room> getAvailableRooms(LocalDate checkIn, LocalDate checkout, boolean isHandicapped, int numberOfPets);

	
	/**
	 * 
	 * @param checkIn
	 * @param checkout
	 * @param isHandicapped
	 * @param numberOfPets
	 * @param numberOfBeds
	 * @return
	 */
	public List<Room> getAvailableRooms(LocalDate checkIn, LocalDate checkout, boolean isHandicapped, int numberOfPets, int numberOfBeds);

}
