package com.connor.dao;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.inject.Produces;

import com.connor.domain.Reservation;
import com.connor.domain.Room;

/**
 * This is a cdi producer for dummy data (to make unit testing easier and to follow ioc and never "new" objects ourselves)
 * @author Connors
 *
 */
public class DummyDataProducer {

	@Produces
	public  List<Room> getDummyRoomData(){
		return new ArrayList<>(); //TODO read in from sys property too
	}
	
	@Produces
	public  List<Reservation> getDummyReservationData(){
		return new ArrayList<>(); //TODO read in from sys property too
	}
}
