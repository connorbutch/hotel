package com.connor.dao;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;

import com.connor.domain.Reservation;
import com.connor.domain.Room;
import com.connor.domain.RoomUtil;
import com.connor.domain.exception.RoomAlreadyBookedException;

/**
 * 
 * @author Connors
 *
 */
public class OriginalDaoImpl implements OriginalDao {

	/**
	 * For serialization
	 */
	private static final long serialVersionUID = 7735992737573699390L;

	/**
	 * The default starting id
	 */
	private static final int DEFAULT_STARTING_ID = 1;

	/**
	 * Act like this came from a db :)
	 */
	private final List<Room> allRooms;

	/**
	 * Act like this came from a db :)
	 */
	private final List<Reservation> allReservations;	

	/**
	 * Utility class
	 */
	private final RoomUtil roomUtil;

	/**
	 * Cdi-enabled constructor.
	 * @param allRooms
	 * @param allReservations
	 * @param roomUtil
	 */
	@Inject
	public OriginalDaoImpl(List<Room> allRooms, List<Reservation> allReservations, RoomUtil roomUtil) {
		this.allRooms = allRooms;
		this.allReservations = allReservations;
		this.roomUtil = roomUtil;
	}

	@Override
	public Reservation placeReservation(List<Room> rooms, LocalDate checkIn, LocalDate checkout, int numberOfPets) {
		Reservation reservation = null;

		//check if room is already booked, if it is, then throw exception
		if(CollectionUtils.isNotEmpty(rooms)) {
			for(Room room: rooms) {
				if(isRoomAlreadyBooked(room.getId(), checkIn, checkout)) {
					throw new RoomAlreadyBookedException("Can't book", room.getId(), checkIn, checkout); //may not be exact range, but can come back
				}
			}
		}

		//if not already booked, create the reservation, save it, and return it for use in response
		reservation = new Reservation();
		reservation.setId(getNextId());
		reservation.setRooms(rooms);
		reservation.setStartDate(checkIn);
		reservation.setEndDate(checkout);


		return reservation;
	}

	@Override
	public List<Room> getAvailableRooms(LocalDate checkIn, LocalDate checkout) {
		//keep a list of the room ids that are available -- default to all rooms
		List<Room> availableRooms = allRooms;

		//remove if a room is booked for even a single day in the range (customer only wants to stay in one place)
		if(CollectionUtils.isNotEmpty(allReservations)) {
			for(Reservation r: allReservations) {
				for(Room room: r.getRooms()) {
					//overrode hashcode and equals to only use id, so this is fine
					allRooms.remove(room);
				}
			}
		}

		//return the rooms that match
		return availableRooms;
	}

	@Override
	public List<Room> getAvailableRooms(LocalDate checkIn, LocalDate checkout, boolean isHandicapped) {
		//find available rooms by calling overloaded method
		List<Room> rooms = getAvailableRooms(checkIn, checkout);

		//then filter out ones that are not handicap accessible if needed
		if(isHandicapped) {
			for(Room room: rooms) {
				if(!roomUtil.isHandicapAccessible(room)) {
					rooms.remove(room);
				}
			}
		}		

		//return the new list
		return rooms;
	}

	@Override
	public List<Room> getAvailableRooms(LocalDate checkIn, LocalDate checkout, boolean isHandicapped,
			int numberOfPets) {
		//call the overloaded method to get rooms
		List<Room> rooms = getAvailableRooms(checkIn, checkout, isHandicapped);

		//then remove it does not allow enough pets
		for(Room room: rooms) {
			if(roomUtil.getNumberOfAllowedPets(room) < numberOfPets) {
				rooms.remove(room);
			}
		}

		//return the cut down list
		return rooms;
	}	

	@Override
	public List<Room> getAvailableRooms(LocalDate checkIn, LocalDate checkout, boolean isHandicapped, int numberOfPets,
			int numberOfBeds) {
		//call the overloaded method to get rooms
		List<Room> rooms = getAvailableRooms(checkIn, checkout, isHandicapped, numberOfPets);

		//remove if does not have enough beds
		for(Room room: rooms) {
			if(room.getNumberOfBeds() < numberOfBeds) {
				rooms.remove(room);
			}
		}

		//return the cut down list
		return rooms;
	}

	/**
	 * Return true if room is booked for one or more days in range between checkin and checkout, else false
	 * @param roomId
	 * @param checkIn
	 * @param checkout
	 * @return
	 */
	private boolean isRoomAlreadyBooked(int roomId, LocalDate checkIn, LocalDate checkout) {
		boolean isFree = false;

		//pretend we null check all of these -- left out for readability for interview :)
		for(Reservation reservation: allReservations) {
			for(Room room: reservation.getRooms()) {
				//if we have match,set to true and break to stop processing
				if(room.getId() == roomId && doDateRangesOverlap(checkIn, checkout, reservation.getStartDate(), reservation.getEndDate())) {
					isFree = true;
					break;
				}
			}
		}

		//return the value
		return isFree;
	}

	/**
	 * Will return true if even one day overlaps between the two.  NOTE: this isn't quite realistic, but you get the point
	 * (as someone can checkout, then someone else can check in later . . . )
	 * @param checkInOne
	 * @param checkoutOne
	 * @param checkInTwo
	 * @param checkoutTwo
	 * @return
	 */
	private boolean doDateRangesOverlap(LocalDate checkInOne, LocalDate checkoutOne, LocalDate checkInTwo, LocalDate checkoutTwo) {
		//would null check in real life		
		//https://stackoverflow.com/questions/325933/determine-whether-two-date-ranges-overlap second answer (400 ish upvotes)
		return (checkInOne.isBefore(checkoutTwo) || checkInOne.isEqual(checkoutTwo)) && (checkInTwo.isBefore(checkoutOne) || checkInTwo.isEqual(checkoutOne));
	}

	/**
	 * Get the next id for the reservations.  Synchronized so we don't get same key generated twice.
	 * @return
	 */
	private synchronized int getNextId() {
		//for first time, just return 1
		if(CollectionUtils.isEmpty(allReservations)) {
			return DEFAULT_STARTING_ID;
		}

		//if not empty, return one more than the previous highest
		return allReservations
				.stream()
				.min(Comparator.comparing(Reservation::getId))
				.orElseThrow() //shouldn't ever happen
				.getId() + 1;
	}


}