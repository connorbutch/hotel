#Author: Connor Butch
#File: Booking.feature
#This feature file is inteneded to outline the integration tests
#and the acceptance criteria for booking rooms.  Please note
#that the future functionality can start to be fleshed out in other feature files.
Feature: Testing booking hotel rooms


#NOTE: for dates, please use m/d/y (month day then year) stripping leading zeros (i.e. Jan 2, 22 is 1/1/22)
#NOTE: for is handicapped, please put y or n for yes is handicapped, and no is not handicapped respecitively
#NOTE: price is always in usd

@PositiveTest
@MakeAReservation
Scenario Outline: If there is at least one room that meets the criteria available, a reservation will be made.
    Given I want to check in on <checkInDate> 
    And I want to check out on <checkOutDate>  
    And I aren/aren't handicapped <isHandicapped>
    And I would like to bring <numberOfPets> pets
    And I would like at least <numberOfBeds> beds
    When I try to place a reservation  
    Then I will get a confirmation screen
    And the price will be <price> 
    
      Examples: 
    |checkInDate|checkOutDate|isHandicapped|numberOfPets|numberOfBeds|price|
    | 1/1/22| 1/4/22 |     n       |     0      |       2    |  1 |
		
		
		 @PositiveTest
		 @DontMakeReservation
    Scenario Outline: If there is no room available that meets the criteria, then a reservation will not be made.
    Given I want to check in on <checkInDate> 
    And I want to check out on <checkOutDate> 
    And I aren/aren't handicapped <isHandicapped> 
    And I would like to bring <numberOfPets> pets
    And I would like at least <numberOfBeds> beds
    When I try to place a reservation    
    Then I will not be allowed to make a reservation
    
     Examples: 
    |checkInDate|checkOutDate|isHandicapped|numberOfPets|numberOfBeds|
    | 2/8/22|2/13/22  |   y         |     2      |   3        |
    
    
    @NegativeTest
		Scenario Outline: If I attempt to check in with bad values, then a reservation will not be made.
		 Given I want to check in on <checkInDate> 
    And I want to check out on <checkOutDate> 
    And I aren/aren't handicapped <isHandicapped> 
    And I would like to bring <numberOfPets> pets
    And I would like at least <numberOfBeds> beds
    When I try to place a reservation    
    Then I will get an error message of <errorMessage>

 Examples: 
    |checkInDate|checkOutDate|isHandicapped|numberOfPets|numberOfBeds|errorMessage|
    | 2/4/2020  | 2/4/2020 |   n         |     1      |  1         |      test  |
