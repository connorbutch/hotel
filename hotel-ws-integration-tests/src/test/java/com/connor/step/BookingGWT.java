package com.connor.step;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.time.DateTimeException;
import java.time.LocalDate;

import com.connor.domain.ErrorResponse;
import com.connor.domain.IHotelConstants;
import com.connor.domain.Reservation;
import com.connor.domain.VariableHolder;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * This class contains the given, when, then (gwt) step definitions
 * @author Connors
 *
 */
public class BookingGWT {

	//the uri for the get request
	private static final String URI_STR = "http://localhost:8080/reservations/";

	//get access to singleton
	private VariableHolder holder = VariableHolder.getInstance();

	//-------------------------------------------------------------------------------------------------
	//these are common to all scenarios
	@Given("I want to check in on {int}\\\\/{int}\\\\/{int}")
	public void iWantToCheckInOnMmddyyyy(Integer int1, Integer int2, Integer int3)  {

	}

	@When("I try to place a reservation")
	public void iTryToPlaceAReservation()  {		
		Client client = ClientBuilder.newClient();
		try {
			//NOTE: this can throw multiple exceptions, but the one we care about is web app, see below for why
			client.target(buildTargetUriWithQueryParams()).request(MediaType.APPLICATION_JSON).get(Reservation.class);
		}catch(WebApplicationException e) {
			//this usually means we did not get the correct class to convert it to, so we should check if it is an error response
			handleWebAppException(e);
		}
	}



	@And("I want to check out on {int}\\\\/{int}\\\\/{int}")
	public void iWantToCheckOutOnMmddyyyy(Integer int1, Integer int2, Integer int3)  {
		//read the date in and store in variable
		LocalDate checkout = null;
		try {
			checkout = LocalDate.of(int1, int2, int3);
		}catch(DateTimeException e) {
			//silence -- they didn't enter date in correct format, should warn in future
		}
		holder.setCheckOut(checkout);		
	}

	@Given("I aren\\/aren't handicapped n")
	public void i_aren_aren_t_handicapped_n() {
		holder.setHandiCapped(false);
	}

	@Given("I aren\\/aren't handicapped y")
	public void i_aren_aren_t_handicapped_y() {
		holder.setHandiCapped(true);
	}

	@And("^I would like to bring (.+) pets$")
	public void iWouldLikeToBringPets(String numberofpets)  {
		int numPets = 0; //default to 0
		try {
			numPets = Integer.parseInt(numberofpets);
		}catch(NumberFormatException e) {
			//silence -- just stay with default
		}

		holder.setNumberOfPets(numPets);
	}

	@And("^I would like at least (.+) beds$")
	public void iWouldLikeAtLeastBeds(String numberofbeds)  {
		int numBeds = 1;
		try {
			numBeds = Integer.parseInt(numberofbeds);
		}catch(NumberFormatException e) {
			//silence -- just stay with default
		}

		holder.setNumberOfBeds(numBeds);
	}


	//-------------------------------------------------------------------------------------------------
	//these are specific to first scenario
	@Then("^I will get a confirmation screen$")
	public void iWillGetAConfirmationScreen()  {
		assertTrue("We should get a response back", holder.getResponseBody() != null);
		assertTrue("The response should be a reservation, not an error", holder.getResponseBody() instanceof Reservation);
	}


	@And("the price will be {int}")
	public void thePriceWillBeUsDollars(Integer int1)  {
		if(int1 != null) {
			assertTrue("Actual and expected prices should be the same", int1 == holder.getActualPrice());
		}
	}

	//-------------------------------------------------------------------------------------------------
	//these specific to second scenario

	@Then("^I will not be allowed to make a reservation$")
	public void iWillNotBeAllowedToMakeAReservation()  {
		//should get null response back
		assertTrue("In the case that no rooms are available, a reservation should not be made", holder.getResponseBody() == null);
	}


	//-------------------------------------------------------------------------------------------------
	//these are specific to third scenario
	@Then("^I will get an error message of (.+)$")
	public void iWillGetAnErrorMessageOf(String errormessage)  {
		assertTrue("Submitting an invalid request should get an error response body of request", holder.getResponseBody() != null);
		assertTrue("Submitting a bad request should get an error response back", holder.getResponseBody() instanceof ErrorResponse);
		ErrorResponse response = (ErrorResponse) holder.getResponseBody();
		assertTrue("The error message returned should match the expected error message", response.getMessage().contains(errormessage));
	}

	/**
	 * Build the uri to make the get request to, including the query params
	 * NOTE: jaxrs does provide a native implementation, but it does not allow for null params
	 * (throws null pointer), so we opt for our own implementation.  Also note that we use the 
	 * same name of the query parameters from the domain class, so we are talking same language 
	 * the service implementation.
	 */
	private String buildTargetUriWithQueryParams() {
		StringBuilder sb = new StringBuilder(URI_STR);

		//query param strings need & between variables if more than one query param is declared
		boolean needsAmpersand = false;

		//check in
		if(holder.getCheckIn() != null) {
			sb.append(IHotelConstants.CHECK_IN_QUERY_PARAM);
			sb.append("=");
			sb.append(holder.getCheckIn());
			needsAmpersand = true;
		}

		//check out
		if(holder.getCheckOut() != null) {
			//add the ampersand if not first query param
			if(needsAmpersand) {
				sb.append("&");
			}
			sb.append(IHotelConstants.CHECK_OUT_QUERY_PARAM);
			sb.append("=");
			sb.append(holder.getCheckOut()); 
			needsAmpersand = true;
		}

		//handicapped
		if(holder.isHandiCapped()) {
			//add the ampersand if not first query param
			if(needsAmpersand) {
				sb.append("&");
			}
			sb.append(IHotelConstants.IS_HANDICAPPED_QUERY_PARAM);
			sb.append("=");
			sb.append(holder.isHandiCapped()); 
			needsAmpersand = true;

		}

		//pets
		if(holder.getNumberOfPets() >= 0) {
			//add the ampersand if not first query param
			if(needsAmpersand) {
				sb.append("&");
			}
			sb.append(IHotelConstants.NUMBER_OF_PETS_QUERY_PARAM);
			sb.append("=");
			sb.append(holder.getNumberOfPets()); 
			needsAmpersand = true;

		}

		//beds
		if(holder.getNumberOfBeds() >= 0) {
			//add the ampersand if not first query param
			if(needsAmpersand) {
				sb.append("&");
			}
			sb.append(IHotelConstants.NUMBER_OF_BEDS_QUERY_PARAM);
			sb.append("=");
			sb.append(holder.getNumberOfBeds()); 
			needsAmpersand = true;

		}

		//build and return the string
		return sb.toString();
	}

	/**
	 * Handles a web app exception, extracted here so that we don't have nested try/catch
	 * @param e
	 */
	private void handleWebAppException(WebApplicationException e) {
		if(e != null && e.getResponse() != null) {
			try {
				ErrorResponse errResponse = e.getResponse().readEntity(ErrorResponse.class);
				holder.setActualErrorMessage(errResponse.getMessage());
				holder.setResponseBody(errResponse);
			}catch(ProcessingException e2) {
				fail("Did not get reservation or error response object as body of response");
			}
		}		
	}
}
