package com.connor.domain;

import java.time.LocalDate;

/**
 * This is a class that contains the variables for access across different classes.
 * This follows the singleton design pattern.
 * @author Connors
 *
 */
public class VariableHolder {

	/**
	 * The singleton.
	 */
	private static VariableHolder instance;
	
	
	/**
	 * Hidden constructor.
	 */
	private VariableHolder() {
		
	}
	
	/**
	 * Get the instance.
	 * @return
	 */
	public static synchronized VariableHolder getInstance() {
		if(instance == null) {
			instance = new VariableHolder();
		}
		
		return instance;
	}
	
	//the fields
	
	private LocalDate checkIn;
	
	private LocalDate checkOut;
	
	private boolean isHandiCapped;
	
	private int numberOfPets;
	
	private int numberOfBeds;
	
	private int expectedPrice;
	
	private int expectedErrorMessage;
	
	/**
	 * NOTE: this can be either Reservation or error respones object
	 */
	private Object responseBody;
	
	//come from the actual response
	private int actualPrice;
	
	private String actualErrorMessage;

	public void setCheckIn(LocalDate checkIn) {
		this.checkIn = checkIn;
	}

	public void setCheckOut(LocalDate checkOut) {
		this.checkOut = checkOut;
	}

	public void setHandiCapped(boolean isHandiCapped) {
		this.isHandiCapped = isHandiCapped;
	}

	public void setNumberOfPets(int numberOfPets) {
		this.numberOfPets = numberOfPets;
	}

	public void setNumberOfBeds(int numberOfBeds) {
		this.numberOfBeds = numberOfBeds;
	}

	public void setExpectedPrice(int expectedPrice) {
		this.expectedPrice = expectedPrice;
	}

	public void setExpectedErrorMessage(int expectedErrorMessage) {
		this.expectedErrorMessage = expectedErrorMessage;
	}

	public LocalDate getCheckIn() {
		return checkIn;
	}

	public LocalDate getCheckOut() {
		return checkOut;
	}

	public boolean isHandiCapped() {
		return isHandiCapped;
	}

	public int getNumberOfPets() {
		return numberOfPets;
	}

	public int getNumberOfBeds() {
		return numberOfBeds;
	}

	public int getExpectedPrice() {
		return expectedPrice;
	}

	public int getExpectedErrorMessage() {
		return expectedErrorMessage;
	}

	public int getActualPrice() {
		return actualPrice;
	}

	public void setActualPrice(int actualPrice) {
		this.actualPrice = actualPrice;
	}

	public String getActualErrorMessage() {
		return actualErrorMessage;
	}

	public void setActualErrorMessage(String actualErrorMessage) {
		this.actualErrorMessage = actualErrorMessage;
	}

	public Object getResponseBody() {
		return responseBody;
	}

	public void setResponseBody(Object responseBody) {
		this.responseBody = responseBody;
	}	
	
	
	
}