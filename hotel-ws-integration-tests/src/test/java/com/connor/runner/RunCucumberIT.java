package com.connor.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

/**
 * This class runs our cucumber integration tests.  This class will be ran automatically via the failsafe plugin
 * when a maven build is executed (mvn integration-test, or any phase after it), or can be run via right click ->
 * run as -> junit.
 * 
 * The output of this test can be viewed in the target -> cucumber-reports folder
 * @author Connors
 *
 */
@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber-reports"},
features = "src/test/resources",
glue="com.connor.step")
public class RunCucumberIT {
	
	@BeforeClass
	public static void init() {
		System.out.println("Welcome to our quick integration tests:)");
	}
	
	@AfterClass
	public static void destroy() {
		System.out.println("Ending integration tests -- hope you had fun:)");
	}
}
