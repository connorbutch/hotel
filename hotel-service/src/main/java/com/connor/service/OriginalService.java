package com.connor.service;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import com.connor.domain.Reservation;
import com.connor.domain.Room;
import com.connor.domain.exception.InvalidDateRangeException;

/**
 * This is the base functionalityu of the service.  We should look to use other services later.  Please discuss with Connor.
 * @author Connors
 *
 */
public interface OriginalService extends Serializable {

	/**
	 * Will first query if any rooms meeting this criteria are available.  If any are, will attempt to book them and return the reservation.
	 * In the case no rooms meeting the criteria are available, will return null.
	 * @param checkIn
	 * @param checkout
	 * @param numberOfPets
	 * @param isHandicapped
	 * @param numberOfBeds
	 * @throws InvalidDateRangeException
	 * @throws TooManyPetsException
	 * @return
	 */
	public Reservation placeReservation(LocalDate checkIn, LocalDate checkout, int numberOfPets, boolean isHandicapped, int numberOfBeds);

	
}
