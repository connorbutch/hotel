package com.connor.service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.collections4.CollectionUtils;

import com.connor.dao.OriginalDao;
import com.connor.domain.Reservation;
import com.connor.domain.Room;
import com.connor.domain.exception.InvalidDateRangeException;
import com.connor.domain.exception.RoomAlreadyBookedException;
import com.connor.domain.exception.TooManyPetsException;

/**
 * Implementation of original service.  
 * 
 * NOTE: could/shoudl likely make ejb in future.
 * @author Connors
 *
 */
public class OriginalServiceImpl implements OriginalService {

	/**
	 * For serialization.
	 */
	private static final long serialVersionUID = 112341231111L;

	/**
	 * Can only stay two weeks.
	 */
	private static final long MAX_LENGTH_OF_STAY_IN_DAYS = 14;
	
	/**
	 * Max number of pets
	 */
	private static final int MAX_NUMBER_OF_PETS = 2;

	/**
	 * The dao
	 */
	private final OriginalDao dao;


	/**
	 * Cdi-enabled constructor.
	 * @param dao
	 */
	@Inject
	public OriginalServiceImpl(OriginalDao dao) {
		this.dao = dao;
	}

	@Override
	public Reservation placeReservation(LocalDate checkIn, LocalDate checkout, int numberOfPets,
			boolean isHandicapped, int numberOfBeds) {
		//what we return
		Reservation reservation = null;

		//validate the input, throw exception if invalid and let go up call stack
		validateDates(checkIn, checkout);
		
		//validate number of pets, throw exception if too many
		if(numberOfPets > MAX_NUMBER_OF_PETS) {
			String errorMessage = String.format("Can only bring %d pets", MAX_NUMBER_OF_PETS);
			throw new TooManyPetsException(errorMessage, numberOfPets);
		}

		//get the available rooms
		List<Room> availableRooms = dao.getAvailableRooms(checkIn, checkout, isHandicapped, numberOfPets, numberOfBeds);

		//if we have at least one, then book the first available (in future, could update to loop over in case someone purchases in meantime)
		if(CollectionUtils.isNotEmpty(availableRooms)) {
			for(Room room: availableRooms) {
				List<Room> roomsToReserve = Arrays.asList(room);
				try {
					reservation = dao.placeReservation(roomsToReserve, checkIn, checkout, numberOfPets);
				}
				//this would happen if someone booked a room at pretty much the exact same time as us -- in future, could loop over all rooms before returning
				catch(RoomAlreadyBookedException e) {
					//silence, not really an error -- see above comment
				}
			}
		}

		//return the newly placed reservation (or null if none were available)
		return reservation;
	}

	/**
	 * Validates dates -- throws invalid date range exception if needed
	 * @param checkIn
	 * @param checkout
	 * @throws InvalidDateRangeException
	 */
	private void validateDates(LocalDate checkIn, LocalDate checkout) {
		//neither can be null
		if(checkIn == null || checkout == null) {
			throw new InvalidDateRangeException("Can't be null", checkIn, checkout);
		}

		//can't check in "in the past"
		if(checkIn.isBefore(LocalDate.now())) {
			throw new InvalidDateRangeException("Can't check in in the past", checkIn, checkout);
		}

		//can't stay for more than 2 weeks -- business logic
		if(checkIn.plusDays(MAX_LENGTH_OF_STAY_IN_DAYS).isBefore(checkout)) {
			String errorMessage = String.format("Can only stay %d days", MAX_LENGTH_OF_STAY_IN_DAYS);
			throw new InvalidDateRangeException(errorMessage, checkIn, checkout);
		}

		//if we reached here -- all good, dates are valid
	}
}
